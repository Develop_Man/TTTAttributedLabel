Pod::Spec.new do |s|
  s.name = 'TTTAttributedLabel'
  s.version = '1.10.0A'
  s.authors = {'Mattt Thompson' => 'm@mattt.me'}
  s.homepage = 'http://git.oschina.net/zhfish/TTTAttributedLabel'
  s.social_media_url = 'https://twitter.com/mattt'
  s.summary = 'A drop-in replacement for UILabel that supports attributes, data detectors, links, and more.'
  s.source = {:git => 'http://git.oschina.net/zhfish/TTTAttributedLabel.git', :tag => '1.10.0A'}
  s.license = 'MIT'

  s.requires_arc = true

  s.platform = :ios
  s.ios.deployment_target = '4.3'

  s.frameworks = 'CoreText', 'CoreGraphics', 'QuartzCore'
  s.source_files = 'TTTAttributedLabel'
end
